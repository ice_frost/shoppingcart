package test.cart;

public class OneGBDataPackPerItem implements Promotion {

	@Override
	public Double apply(ShoppingCart cart, ShoppingCartItem item) {
		Product product = cart.getPricingRules().getProduct("1gb");
		ShoppingCartItem promoItem = new ShoppingCartFreebieItem(cart, product, item.getQuantity());
		cart.add(promoItem);

		return 0.0;
	}

	@Override
	public boolean isGlobal() {
		return false;
	}

}
