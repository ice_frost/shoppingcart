package test.cart;

public class ThreeForTwo implements Promotion {

	@Override
	public Double apply(ShoppingCart cart, ShoppingCartItem item) {
		if (item.getQuantity() > 2) {
			return (item.getQuantity() / 3) * item.pricePerItem();
		}

		return 0.0;
	}

	@Override
	public boolean isGlobal() {
		return false;
	}

}
