package test.cart;

public class ILoveAmaysim implements Promotion {
	
	private Double promoDiscountPercentage;
	
	/**
	 * @param promoDiscountPercentage promo discount percentage
	 */
	public ILoveAmaysim(Double promoDiscountPercentage) {
		this.promoDiscountPercentage = promoDiscountPercentage / 100;
	}

	@Override
	public Double apply(ShoppingCart cart, ShoppingCartItem item) {
		return item.getRegularPrice() * promoDiscountPercentage;
	}

	@Override
	public boolean isGlobal() {
		return true;
	}

}
