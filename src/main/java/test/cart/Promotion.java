package test.cart;

/**
 * Interface for all promotions 
 */
public interface Promotion {
	
	/**
	 * Apply promotion to cart and/or items. Returns discount earned for item
	 * 
	 * @param cart
	 * @param item
	 * @return discount amount
	 */
	Double apply(ShoppingCart cart, ShoppingCartItem item);
	
	/**
	 * flag that indicates if promo is applicable to all items in cart
	 * 
	 * @return true if applicable to all items, false otherwise
	 */
	boolean isGlobal();

}
