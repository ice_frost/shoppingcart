package test.cart;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartItem {

	private ShoppingCart cart;
	private Product product;
	private int quantity;
	private List<Promotion> promotions;

	public ShoppingCartItem(ShoppingCart cart, Product product) {
		this(cart, product, 1);
	}

	public ShoppingCartItem(ShoppingCart cart, Product product, int quantity) {
		this.cart = cart;
		this.product = product;
		this.quantity = quantity;
		promotions = new ArrayList<>();
	}

	public String getItemCode() {
		return product.getProductCode();
	}

	public int getQuantity() {
		return quantity;
	}

	public Double pricePerItem() {
		return product.getPrice();
	}

	public Double getRegularPrice() {
		return product.getPrice() * quantity;
	}

	public Double getTotal() {
		double totalDisCount = 0.0;
		for (Promotion promotion : promotions) {
			totalDisCount += promotion.apply(cart, this);
		}
		return getRegularPrice() - totalDisCount;
	}

	public void incrementQuantity() {
		++quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShoppingCartItem other = (ShoppingCartItem) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	public void addPromotion(Promotion promo) {
		if (!promotions.contains(promo)) {
			promotions.add(promo);
		}

	}

}
