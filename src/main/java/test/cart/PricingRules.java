package test.cart;

import java.util.HashMap;
import java.util.Map;

public class PricingRules {

	private Map<String, Product> products;
	private Map<String, Promotion> promotions;

	public PricingRules() {
		products = new HashMap<>();
		promotions = new HashMap<>();

		products.put("ult_small", new Product("ult_small", "Unlimited 1GB", 24.90));
		products.put("ult_medium", new Product("ult_medium", "Unlimited 2GB", 29.90));
		products.put("ult_large", new Product("ult_large", "Unlimited 5GB", 44.90));
		products.put("1gb", new Product("1gb", "1 GB Data-pack", 9.90));

		promotions.put("342", new ThreeForTwo());
		promotions.put("bulk", new BulkDiscount(39.9));
		promotions.put("1gdppi", new OneGBDataPackPerItem());
		promotions.put("I<3AMAYSIM", new ILoveAmaysim(10.0));

	}

	public Product getProduct(String productCode) {
		return products.get(productCode);
	}

	public Promotion getPromotion(String promoCode) {
		return promotions.get(promoCode);
	}

}
