package test.cart;

public class ShoppingCartFreebieItem extends ShoppingCartItem {

	public ShoppingCartFreebieItem(ShoppingCart cart, Product product) {
		super(cart, product);
	}

	public ShoppingCartFreebieItem(ShoppingCart cart, Product product, int quantity) {
		super(cart, product, quantity);
	}

	@Override
	public Double getTotal() {
		return 0.0;
	}

}
