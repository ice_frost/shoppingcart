package test.cart;

public class BulkDiscount implements Promotion {
	
	private Double bulkPerItemPrice;
	
	public BulkDiscount(Double bulkPerItemPrice) {
		this.bulkPerItemPrice = bulkPerItemPrice;
	}

	@Override
	public Double apply(ShoppingCart cart, ShoppingCartItem item) {
		if (item.getQuantity() > 3) {
			return item.getRegularPrice() - (item.getQuantity() * bulkPerItemPrice);
		}

		return 0.0;
	}

	@Override
	public boolean isGlobal() {
		return false;
	}

}
