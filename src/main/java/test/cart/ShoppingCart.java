package test.cart;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ShoppingCart {

	private PricingRules pricingRules;
	private Map<String, ShoppingCartItem> items;
	private DecimalFormat df = new DecimalFormat("#.00");
	private Set<Promotion> cartWidePromos;

	public ShoppingCart(PricingRules pricingRules) {
		this.pricingRules = pricingRules;
		items = new ConcurrentHashMap<>();
		cartWidePromos = new HashSet<>();
	}

	public PricingRules getPricingRules() {
		return pricingRules;
	}

	public void add(ShoppingCartItem item) {
    	        if (!cartWidePromos.isEmpty()) {
                    for (Promotion promo: cartWidePromos) {
                        item.addPromotion(promo);
                    }
                }
		addItemToCart(item);
	}

	public void add(ShoppingCartItem item, String promoCode) {
		Promotion promo = pricingRules.getPromotion(promoCode);
		if (promo.isGlobal()) {
			cartWidePromos.add(promo);
			if (!items.isEmpty()) {
				for (ShoppingCartItem cartItem : items.values()) {
					cartItem.addPromotion(promo);
				}
			}
		}
		item.addPromotion(promo);
		addItemToCart(item);
	}

	public Double total() {
		double total = 0.0;
		for (ShoppingCartItem item : items.values()) {
			total += item.getTotal();
		}
		return new Double(df.format(total));
	}

	public Collection<ShoppingCartItem> items() {
		return Collections.unmodifiableCollection(items.values());
	}

	public int totalItems() {
		int total = 0;
		for (ShoppingCartItem item : items.values()) {
			total += item.getQuantity();
		}
		return total;
	}

	private void addItemToCart(ShoppingCartItem item) {
		if (items.containsKey(item.getItemCode())) {
			items.get(item.getItemCode()).incrementQuantity();
		} else {
			items.put(item.getItemCode().toString(), item);
		}
	}
}
