package test.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PricingRulesTest {

	@Test
	public void test() {
		PricingRules rules = new PricingRules();

		String productCode = "ult_small";
		Product product = rules.getProduct(productCode);
		assertEquals(productCode, product.getProductCode());
		assertEquals("Unlimited 1GB", product.getProductName());
		assertEquals(new Double(24.90), product.getPrice());

		productCode = "ult_medium";
		product = rules.getProduct(productCode);
		assertEquals(productCode, product.getProductCode());
		assertEquals("Unlimited 2GB", product.getProductName());
		assertEquals(new Double(29.90), product.getPrice());

		productCode = "ult_large";
		product = rules.getProduct(productCode);
		assertEquals(productCode, product.getProductCode());
		assertEquals("Unlimited 5GB", product.getProductName());
		assertEquals(new Double(44.90), product.getPrice());

		productCode = "1gb";
		product = rules.getProduct(productCode);
		assertEquals(productCode, product.getProductCode());
		assertEquals("1 GB Data-pack", product.getProductName());
		assertEquals(new Double(9.90), product.getPrice());

		String promoCode = "342";
		Promotion promo = rules.getPromotion(promoCode);
		assertTrue(promo instanceof ThreeForTwo);

		promoCode = "bulk";
		promo = rules.getPromotion(promoCode);
		assertTrue(promo instanceof BulkDiscount);

		promoCode = "1gdppi";
		promo = rules.getPromotion(promoCode);
		assertTrue(promo instanceof OneGBDataPackPerItem);

		promoCode = "I<3AMAYSIM";
		promo = rules.getPromotion(promoCode);
		assertTrue(promo instanceof ILoveAmaysim);

	}

}
