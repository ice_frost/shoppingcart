package test.cart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShoppingCartItemTest {

	@Test
	public void test() {
		PricingRules rules = new PricingRules();
		Product product = new Product("productCode", "productName", 10.0);
		ShoppingCart cart = new ShoppingCart(rules);
		ShoppingCartItem item = new ShoppingCartItem(cart, product);

		assertEquals(product.getProductCode(), item.getItemCode());
		assertEquals(item.getQuantity(), 1);
		assertEquals(item.pricePerItem(), new Double(10));
		assertEquals(item.getRegularPrice(), new Double(product.getPrice() * item.getQuantity()));

		item.incrementQuantity();
		assertEquals(item.getQuantity(), 2);
		assertEquals(item.pricePerItem(), new Double(10));
		assertEquals(item.getRegularPrice(), new Double(product.getPrice() * item.getQuantity()));

		ShoppingCartItem item2 = new ShoppingCartItem(cart, product);
		assertEquals(item, item2);

	}

}
