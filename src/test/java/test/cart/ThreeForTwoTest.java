package test.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class ThreeForTwoTest {

	@Test
	public void test() {
		PricingRules rules = new PricingRules();
		Product product = new Product("productCode", "productName", 10.0);
		ShoppingCart cart = new ShoppingCart(rules);
		ShoppingCartItem item = new ShoppingCartItem(cart, product);
		item.incrementQuantity();

		ThreeForTwo promo = new ThreeForTwo();

		Double discount = promo.apply(cart, item);
		assertEquals(new Double(0), discount);

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(10), discount);

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(10), discount);

		item.incrementQuantity();
		item.incrementQuantity();
		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(20), discount);

		assertFalse(promo.isGlobal());

	}

}
