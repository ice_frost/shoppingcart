package test.cart;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;

import org.junit.Test;

public class ShoppingCartTest {

	@Test
	public void test() {
		PricingRules pricingRules = new PricingRules();
		ShoppingCart cart = new ShoppingCart(pricingRules);

		assertEquals(0, cart.items().size());

		String productCode = "product1";
		String productName = "productName1";
		Double price = 10.0;
		Product product = new Product(productCode, productName, price);
		ShoppingCartItem item = new ShoppingCartItem(cart, product);

		cart.add(item);
		assertEquals(1, cart.items().size());
		ShoppingCartItem cartItem = cart.items().iterator().next();
		assertEquals(productCode, cartItem.getItemCode());
		assertEquals(price, cart.total());

		item.incrementQuantity();
		assertEquals(1, cart.items().size());
		assertEquals(2, cart.totalItems());
		price = new Double(price * 2);
		assertEquals(price, cart.total());

		String productCode2 = "product2";
		String productName2 = "productName2";
		Double price2 = 20.0;
		product = new Product(productCode2, productName2, price2);
		item = new ShoppingCartItem(cart, product);

		cart.add(item);
		assertEquals(2, cart.items().size());
		assertEquals(3, cart.totalItems());
		Iterator<ShoppingCartItem> iterator = cart.items().iterator();
		iterator.next();
		cartItem = iterator.next();
		assertEquals(productCode, cartItem.getItemCode());
		assertEquals(new Double(price + cartItem.getTotal()), cart.total());

		item.incrementQuantity();
		assertEquals(2, cart.items().size());
		assertEquals(4, cart.totalItems());
		price2 = new Double(price2 * 2);
		assertEquals(new Double(price + price2), cart.total());

	}

}
