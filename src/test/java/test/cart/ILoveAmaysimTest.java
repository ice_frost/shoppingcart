package test.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ILoveAmaysimTest {

	@Test
	public void test() {
		PricingRules rules = new PricingRules();
		Product product = new Product("productCode", "productName", 50.0);
		ShoppingCart cart = new ShoppingCart(rules);
		ShoppingCartItem item = new ShoppingCartItem(cart, product);

		ILoveAmaysim promo = new ILoveAmaysim(10.0);

		Double discount = promo.apply(cart, item);
		assertEquals(new Double(5), discount);

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(10), discount);

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(15), discount);

		assertTrue(promo.isGlobal());
	}

}
