package test.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ProgrammingExercise {

	private PricingRules pricingRules = new PricingRules();

	private Product product1;
	private Product product2;
	private Product product3;
	private Product product4;

	@Before
	public void setup() {
		product1 = pricingRules.getProduct("ult_small");
		product2 = pricingRules.getProduct("ult_medium");
		product3 = pricingRules.getProduct("ult_large");
		product4 = pricingRules.getProduct("1gb");
	}

	@Test
	public void testScenario1() {
		ShoppingCart cart = new ShoppingCart(pricingRules);

		ShoppingCartItem item1 = new ShoppingCartItem(cart, product1);
		cart.add(new ShoppingCartItem(cart, product1), "342");
		cart.add(new ShoppingCartItem(cart, product1));
		cart.add(new ShoppingCartItem(cart, product1));

		ShoppingCartItem item2 = new ShoppingCartItem(cart, product3);
		cart.add(item2);

		assertEquals(4, cart.totalItems());
		assertEquals(new Double(94.70), cart.total());
		assertTrue(cart.items().contains(item1));
		assertTrue(cart.items().contains(item2));
		for (ShoppingCartItem item : cart.items()) {
			if (item.equals(item1)) {
				assertEquals(3, item.getQuantity());
			}
			if (item.equals(item2)) {
				assertEquals(1, item.getQuantity());
			}
		}

	}

	@Test
	public void testScenario2() {
		ShoppingCart cart = new ShoppingCart(pricingRules);

		ShoppingCartItem item1 = new ShoppingCartItem(cart, product1);
		cart.add(new ShoppingCartItem(cart, product1));
		cart.add(new ShoppingCartItem(cart, product1));

		ShoppingCartItem item2 = new ShoppingCartItem(cart, product3);
		cart.add(new ShoppingCartItem(cart, product3), "bulk");
		cart.add(new ShoppingCartItem(cart, product3));
		cart.add(new ShoppingCartItem(cart, product3));
		cart.add(new ShoppingCartItem(cart, product3));

		assertEquals(6, cart.totalItems());
		assertEquals(new Double(209.40), cart.total());
		assertTrue(cart.items().contains(item1));
		assertTrue(cart.items().contains(item2));
		for (ShoppingCartItem item : cart.items()) {
			if (item.equals(item1)) {
				assertEquals(2, item.getQuantity());
			}
			if (item.equals(item2)) {
				assertEquals(4, item.getQuantity());
			}
		}

	}

	@Test
	public void testScenario3() {
		ShoppingCart cart = new ShoppingCart(pricingRules);

		ShoppingCartItem item1 = new ShoppingCartItem(cart, product1);
		cart.add(new ShoppingCartItem(cart, product1));

		ShoppingCartItem item2 = new ShoppingCartItem(cart, product2);
		cart.add(new ShoppingCartItem(cart, product2), "1gdppi");
		cart.add(new ShoppingCartItem(cart, product2));

		ShoppingCartItem promoItem = new ShoppingCartFreebieItem(cart, product4);

		assertEquals(new Double(84.70), cart.total());
		assertEquals(5, cart.totalItems());
		assertTrue(cart.items().contains(item1));
		assertTrue(cart.items().contains(item2));
		assertTrue(cart.items().contains(promoItem));
		for (ShoppingCartItem item : cart.items()) {
			if (item.equals(item1)) {
				assertEquals(1, item.getQuantity());
			}
			if (item.equals(item2)) {
				assertEquals(2, item.getQuantity());
			}
			if (item.equals(promoItem)) {
				assertEquals(2, item.getQuantity());
			}
		}

	}

	@Test
	public void testScenario4() {
		ShoppingCart cart = new ShoppingCart(pricingRules);

		ShoppingCartItem item1 = new ShoppingCartItem(cart, product1);
		cart.add(new ShoppingCartItem(cart, product1));

		ShoppingCartItem item2 = new ShoppingCartItem(cart, product4);
		cart.add(new ShoppingCartItem(cart, product4), "I<3AMAYSIM");

		assertEquals(2, cart.totalItems());
		assertEquals(new Double(31.32), cart.total());
		assertTrue(cart.items().contains(item1));
		assertTrue(cart.items().contains(item2));
		for (ShoppingCartItem item : cart.items()) {
			if (item.equals(item1)) {
				assertEquals(1, item.getQuantity());
			}
			if (item.equals(item2)) {
				assertEquals(1, item.getQuantity());
			}
		}

	}

}
