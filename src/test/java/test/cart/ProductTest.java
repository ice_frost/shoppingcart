package test.cart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProductTest {

	@Test
	public void test() {
		String productCode = "product1";
		String productName = "productName1";
		Double price = 10.0;
		Product product = new Product(productCode, productName, price);

		assertEquals(productCode, product.getProductCode());
		assertEquals(productName, product.getProductName());
		assertEquals(price, product.getPrice());

	}

}
