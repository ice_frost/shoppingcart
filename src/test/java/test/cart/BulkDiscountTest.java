package test.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class BulkDiscountTest {

	@Test
	public void test() {
		PricingRules rules = new PricingRules();
		Product product = new Product("productCode", "productName", 50.0);
		ShoppingCart cart = new ShoppingCart(rules);
		ShoppingCartItem item = new ShoppingCartItem(cart, product);
		item.incrementQuantity();

		BulkDiscount promo = new BulkDiscount(39.9);

		Double discount = promo.apply(cart, item);
		assertEquals(new Double(0), discount);

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(0), discount);

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(new Double(40.4), discount, 0.01);

		assertFalse(promo.isGlobal());

	}

}
