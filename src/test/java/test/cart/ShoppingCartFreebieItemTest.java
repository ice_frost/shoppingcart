package test.cart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ShoppingCartFreebieItemTest {

	@Test
	public void testGetTotal() {
		PricingRules rules = new PricingRules();
		Product product = new Product("productCode", "productName", 10.0);
		ShoppingCartItem item = new ShoppingCartFreebieItem(new ShoppingCart(rules), product);

		assertEquals(new Double(0), item.getTotal());
		item.incrementQuantity();
		assertEquals(new Double(0), item.getTotal());

		item = new ShoppingCartFreebieItem(new ShoppingCart(rules), product, 3);
		assertEquals(3, item.getQuantity());

	}

}
