package test.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class OneGBDataPackPerItemTest {

	@Test
	public void test() {
		PricingRules rules = new PricingRules();
		Product product = new Product("productCode", "productName", 50.0);
		ShoppingCart cart = new ShoppingCart(rules);
		ShoppingCartItem item = new ShoppingCartItem(cart, product);
		cart.add(item);

		ShoppingCartItem promoItem = new ShoppingCartFreebieItem(cart, rules.getProduct("1gb"));
		OneGBDataPackPerItem promo = new OneGBDataPackPerItem();

		Double discount = promo.apply(cart, item);
		assertEquals(2, cart.totalItems());
		assertEquals(new Double(0), discount);
		assertTrue(cart.items().contains(promoItem));

		item.incrementQuantity();
		discount = promo.apply(cart, item);
		assertEquals(4, cart.totalItems());
		assertEquals(new Double(0), discount);

		assertTrue(cart.items().contains(promoItem));

		assertFalse(promo.isGlobal());

	}

}
